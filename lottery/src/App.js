import React, { Component } from 'react';
import './App.css';
import Lottery from './Lottery/Lottery';

class App extends Component {
  render() {
    return (
      <div className="App">
          <Lottery heading="Лоттерея 5 из 36" />
      </div>
    );
  }
}

export default App;
