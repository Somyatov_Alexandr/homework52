import React, {Component} from "react";
import './Lottery.css';
import LotterItem from "../LotteryItem/LotteryItem";


class Lottery extends Component {
  state = {
    randomArr : [0,0,0,0,0]
  };

  render() {
    return (
        <div className="lottery">
          <h2 className="lottery__heading">{this.props.heading}</h2>
          <button onClick={this.getRandomList} className="waves-effect waves-light btn green" id="add-numbers">
            New numbers
          </button>
          <div className="lottery__counts">
            <LotterItem item={this.state.randomArr[0]} />
            <LotterItem item={this.state.randomArr[1]} />
            <LotterItem item={this.state.randomArr[2]} />
            <LotterItem item={this.state.randomArr[3]} />
            <LotterItem item={this.state.randomArr[4]} />
          </div>
        </div>
    );
  }

  getRandomNum = (min, max) => {
    return Math.floor(Math.random() * (max - min)) + min;
  }

  getRandomList = () => {

    let randomList = [];
    let count = 5;

    while (count > 0) {
      let randomNum = this.getRandomNum(1, 36);

      if (randomList.indexOf(randomNum) === -1) {
        randomList.push(randomNum);
        count--;
      }
    }

    randomList.sort(function (a, b) {
      return a - b;
    });

    this.setState({
      randomArr: randomList
    });
  }

};

export default Lottery;